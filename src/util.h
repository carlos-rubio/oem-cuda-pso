/* 
    OEM-CUDA-PSO - Output Error Method with CUDA and PSO
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef UTIL_H
#define UTIL_H

#include "variables.h"
#include <curand_kernel.h>

// ---------------------------- Host functions --------------------------------

/**
 * removes empty characters from the beginning of a string
 *   tabs, new line, carriage return, spaces and page breaks
 */
char* ltrim(char* str);

/**
 * removes empty characters from the end of a string
 *   tabs, new line, carriage return, spaces and page breaks
 */
char* rtrim(char* str);

/**
 * removes empty characters from both sides of a string
 *   tabs, new line, carriage return, spaces and page breaks
 */
char* trim(char* str);

/**
 * prints pretty time given it in ms
 * writes result in buffer 
 */
void formatTime(char* buffer, float ms);

// ---------------------------- Host functions --------------------------------


// ---------------------------- Device functions --------------------------------

/**
 * device fn to access a variable (as a float[])
 */
__forceinline__ __device__ float* variable(int index, Variables variables){    
    return &(variables.data[index*variables.size]);
}

__forceinline__ __device__ void lock(int* mutex){
    while( atomicCAS(mutex, 0, 1) != 0);
}

__forceinline__ __device__ void unlock(int* mutex){
    atomicExch(mutex, 0);
}

/**
 * Return a random number between [start,end] using 
 * an uniform distribution
 */
__forceinline__ __device__ float randomUniform(curandState* state, float start, float end){
   return start + curand_uniform(state) * (end - start);   
}

/**
 * Return a random integer from 0 to end-1
 * using an uniform distribution
 */
__forceinline__ __device__ float randomInt(curandState* state, int endNoInclusive){
   return curand(state) % endNoInclusive;   
}

/**
 * Copy one position into another
 */
__forceinline__ __device__ void copyPosition(Position from, Position to, int dimensions){
    for(int i=0;i<dimensions;i++) to[i] = from[i];
}

// ---------------------------- Device functions --------------------------------

#endif