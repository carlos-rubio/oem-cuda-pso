/* 
    OEM-CUDA-PSO - Output Error Method with CUDA and PSO
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "config.h"
#include <stdio.h>
#include "kernel.h"

/**
 * Print configuration
 */
void printConfig(Config config){
    printf("\nConfiguration: \n");      
    printf("  Group: %s \n",groupNames[config.group]);      
    printf("  Kernel: %s \n",kernelNames[config.kernel]);
    printf("  Swarms: %d \n",config.swarms); 
    printf("  Particles: %d \n",config.particles); 
    printf("  c1: %f \n",config.c1); 
    printf("  c2: %f \n",config.c2);
    printf("  c3: %f \n",config.c3);
    printf("  Max iterations: %d \n",config.maxIter); 
    printf("  Cost limit to export: %f \n",config.costLimit); 
    printf("  Size limit to export: %d \n",config.sizeLimit); 
    printf("  Informant size: %d \n",config.informantSize);     
}

