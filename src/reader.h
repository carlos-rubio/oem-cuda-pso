/* 
    OEM-CUDA-PSO - Output Error Method with CUDA and PSO
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef READER_H
#define READER_H

#include "variables.h"

/**
 * reads and parse input file
 * allocate memory for the input structure and fills it
 */
void readFile(const char* fileName,Input* input);

/**
 * extracts the configuration part of the Input structure
 */
Config extractConfig(Input input);

/**
 * extracts the parameters part of the Input structure
 * pre-calculate the slope of each axis line
 * allocate memory for the returned parameters
 */
Info extractParameters(Input input);

/**
 * extracts the variables part of the Input structure
 * allocate memory for the returned variables
 */
Variables extractVariables(Input input);

#endif