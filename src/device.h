/* 
    OEM-CUDA-PSO - Output Error Method with CUDA and PSO
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef DEVICE_H
#define DEVICE_H

/**
 * prints some info of all detected CUDA GPUs
 */
void printDevices(void);

/**
 * prints some info of one local CUDA GPUs
 */
void printDevice(int device);

/**
 * selects the GPU with the max number of multiprocessors
 */
int selectBestDevice();

/**
 * gets the maximum number of threads per block of one local CUDA GPU
 */
int getMaxThreadsPerBlock(int device);

#endif