/* 
    OEM-CUDA-PSO - Output Error Method with CUDA and PSO
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef HYPERCUBE_H
#define HYPERCUBE_H

#include "variables.h"

typedef struct {    
    int used;       // number of filled points    
    bool overflow;  // found more lower values than capacity
    } HypercubeInfo;

// ---------------------------- Host functions --------------------------------

/**
 * Returns in memory size of the information part of hypercubes object
 */
size_t hyperInfoSize(Config config);

/**
 * Returns in memory size of the data part of hypercubes object
 */
size_t hyperDataSize(Config config);

/**
 * Returns in memory size to store the minimum position within its cost for each swarm
 */
size_t globalSize(Config config);

/**
 * Exports the hypercubes
 * each point is exported with the format and endianess of the local machine
 * in linux with Intel/AMD64 arch, each point is:
 *   4 bytes, IEE754 simple precision and little-endian
 */
void exportHypercubes(HypercubeInfo* hypercubes, float* data, float* global, const char* fileName, Config config);

/** 
 * Print a summary of each hypercube
 */
void printSummary(HypercubeInfo* hypercubes, float* data, float* global, Config config);

// ---------------------------- Host functions --------------------------------

// ---------------------------- Device functions --------------------------------

/**
 * Returns in memory size of the information part of hypercubes object
 */
__forceinline__ __device__ size_t hyperInfoSizeDev(Config config){
    return config.swarms * sizeof(HypercubeInfo);
}

// ---------------------------- Device functions --------------------------------


#endif