/* 
    OEM-CUDA-PSO - Output Error Method with CUDA and PSO
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "util.h"
#include <stdio.h>
#include <string.h>

static const char *seps = "\t\n\v\f\r ";

/**
 * removes empty characters from the beginning of a string
 *   tabs, new line, carriage return, spaces and page breaks
 */
char* ltrim(char* str) {
    size_t ntrim = strspn(str, seps);
    size_t len = strlen(str);        
    if (ntrim > 0) {        
        if (ntrim == len) str[0] = '\0';
        else memmove(str, str + ntrim, len + 1 - ntrim);        
    }
    return str;
}

/**
 * removes empty characters from the end of a string
 *   tabs, new line, carriage return, spaces and page breaks
 */
char* rtrim(char* str){    
    int pos = strlen(str) - 1;
    while (pos >= 0 && strchr(seps, str[pos]) != NULL) {
        str[pos] = '\0';
        pos--;
    }
    return str;
}

/**
 * removes empty characters from both sides of a string
 *   tabs, new line, carriage return, spaces and page breaks
 */
char* trim(char* str) {
    return ltrim(rtrim(str));
}

/**
 * prints pretty time given it in ms
 * writes result in buffer
 */
void formatTime(char* buffer, float ms){
    float MS = 1.0;
    float SEC = 1000.0 * MS;
    float MIN = 60.0 * SEC;
    float HOUR = 60.0 * MIN;
    float DAY = 24 * HOUR;

    if(ms < SEC)
        sprintf(buffer, "%.2f ms", ms);    
    else if(ms < MIN)
        sprintf(buffer, "%.2f seconds", ms/SEC);    
    else if(ms < HOUR)
        sprintf(buffer, "%.2f minutes", ms/MIN);    
    else if(ms < DAY)
        sprintf(buffer, "%.2f hours", ms/HOUR);    
    else
        sprintf(buffer, "%.2f days", ms/DAY);    
}
