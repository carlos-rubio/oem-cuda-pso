/* 
    OEM-CUDA-PSO - Output Error Method with CUDA and PSO
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "memory.h"
#include "macro.h"
#include "variables.h"
#include <stdlib.h>

// --------------- Host ----------------------------

void allocMemoryInput(Input* input){
    size_t sizeParameters = input->numValues * sizeof(float);
    size_t sizeAxis = input->dimensions * sizeof(Axis);
    size_t sizeData = input->numVariables * input->sizeVariables * sizeof(float);

    input->parameters = (float*)malloc(sizeParameters);
    if(NULL ==  input->parameters){
        HANDLE_ERROR(MALLOC_ERROR);
    }
    input->axis = (Axis*)malloc(sizeAxis);
    if(NULL ==  input->axis){
        HANDLE_ERROR(MALLOC_ERROR);
    }
    input->data = (float*)malloc(sizeData);    
    if(NULL ==  input->data){
        HANDLE_ERROR(MALLOC_ERROR);
    }
}

void freeMemoryInput(Input* input){
    free(input->parameters);
    free(input->axis);
    free(input->data);    
} 

void allocParametersHost(Info* parameters){
    // Values
    size_t sizeValues = parameters->numValues * sizeof(float);    
    parameters->values = (float*)malloc(sizeValues);    
    if (NULL == parameters->values){
        HANDLE_ERROR(MALLOC_ERROR);
    }
    // Axis
    size_t sizeAxis = parameters->numAxis * sizeof(Axis);
    parameters->axis = (Axis*)malloc(sizeAxis);
    if (NULL == parameters->axis){
        HANDLE_ERROR(MALLOC_ERROR);
    }
}

void freeParametersHost(Info* parameters){
    free(parameters->values);
    free(parameters->axis);
}

void allocHypercubesHost(HypercubeInfo** hypercubesHostPtr, Config config){    
    *hypercubesHostPtr = (HypercubeInfo*)malloc(hyperInfoSize(config));
    if (NULL == *hypercubesHostPtr){
        HANDLE_ERROR(MALLOC_ERROR);
    }
}

void initHypercubesHost(HypercubeInfo* hypercubesHost, Config config){            
    for(int i=0;i<config.swarms;i++){
        hypercubesHost[i].used = 0;        
        hypercubesHost[i].overflow = false;
    }    
}

void freeHypercubesHost(HypercubeInfo* hypercubesHost){
    free(hypercubesHost);
}

void allocDataHost(float** dataPtr, Config config){
    *dataPtr = (float*)malloc(hyperDataSize(config));
    if (NULL == *dataPtr){
        HANDLE_ERROR(MALLOC_ERROR);
    }
}

void freeDataHost(float* data){
    free(data);
}

void allocGlobalHost(float** globalPtr, Config config){
    *globalPtr = (float*)malloc(globalSize(config));
    if (NULL == *globalPtr){
        HANDLE_ERROR(MALLOC_ERROR);
    }
}

void freeGlobalHost(float* global){
    free(global);
}

void allocVariablesHost(Variables* variables){
    size_t size = variables->num * variables->size * sizeof(float);
    variables->data = (float*)malloc(size);
    if (NULL == variables->data){
        HANDLE_ERROR(MALLOC_ERROR);
    }
}

void freeVariablesHost(Variables* variables){
    free(variables->data);
}

// --------------- Device ----------------------------

void allocHypercubesDevice(HypercubeInfo** hypercubesDevicePtr, Config config){    
    HANDLE_CUDA( cudaMalloc( hypercubesDevicePtr, hyperInfoSize(config) ) );
}

void freeHypercubesDevice(HypercubeInfo* hypercubesDevice){
    HANDLE_CUDA( cudaFree( hypercubesDevice) );
}

void allocDataDevice(float** dataDevicePtr, Config config){
    HANDLE_CUDA( cudaMalloc( dataDevicePtr, hyperDataSize(config) ) );
}

void freeDataDevice(float* dataDevice){
    HANDLE_CUDA( cudaFree( dataDevice) );
}

void allocGlobalDevice(float** globalDevicePtr, Config config){
    HANDLE_CUDA( cudaMalloc( globalDevicePtr, globalSize(config) ) );
}

void freeGlobalDevice(float* globalDevice){
    HANDLE_CUDA( cudaFree( globalDevice) );
}

void freeVariablesDevice(Variables* variables){
    HANDLE_CUDA( cudaFree(variables->data) );
}

void freeParamsDevice(Info* parametersDevice){
    HANDLE_CUDA( cudaFree( parametersDevice->values) );
    HANDLE_CUDA( cudaFree( parametersDevice->axis) );    
}

// -------- Copy host to device -------------------

void copyHypercubesToDevice(HypercubeInfo* hypercubesHost, HypercubeInfo* hypercubesDevice, Config config){
    size_t sizeInfo = hyperInfoSize(config);
    HANDLE_CUDA( cudaMemcpy( hypercubesDevice, hypercubesHost, sizeInfo, cudaMemcpyHostToDevice) );
}

void copyParametersToDevice(Info* parameters, Info* parametersDevice){
    size_t sizeValues = parameters->numValues * sizeof(float);        
    HANDLE_CUDA( cudaMalloc( &parametersDevice->values, sizeValues) );
    HANDLE_CUDA( cudaMemcpy( parametersDevice->values, parameters->values, sizeValues, cudaMemcpyHostToDevice) );

    size_t sizeAxis = parameters->numAxis * sizeof(Axis);   
    HANDLE_CUDA( cudaMalloc( &parametersDevice->axis, sizeAxis) );
    HANDLE_CUDA( cudaMemcpy( parametersDevice->axis, parameters->axis, sizeAxis, cudaMemcpyHostToDevice) );
}

void copyVariablesToDevice(Variables* variables, Variables* variablesDevice){
    size_t size = variables->num * variables->size * sizeof(float);    
    HANDLE_CUDA( cudaMalloc( &variablesDevice->data, size) );
    HANDLE_CUDA( cudaMemcpy( variablesDevice->data , variables->data, size, cudaMemcpyHostToDevice) );    
}

// ------------ Copy device to Host -----------

void copyHypercubesToHost(HypercubeInfo* hypercubesDevice, HypercubeInfo* hypercubesHost, Config config){
    HANDLE_CUDA( cudaMemcpy( hypercubesHost, hypercubesDevice, hyperInfoSize(config), cudaMemcpyDeviceToHost) );
}

void copyDataToHost(float* dataDevice, float* dataHost, Config config){
    HANDLE_CUDA( cudaMemcpy( dataHost, dataDevice, hyperDataSize(config), cudaMemcpyDeviceToHost) );
}

void copyGlobalToHost(float* globalDevice, float* globalHost, Config config){
    HANDLE_CUDA( cudaMemcpy( globalHost, globalDevice, globalSize(config), cudaMemcpyDeviceToHost) );
}
