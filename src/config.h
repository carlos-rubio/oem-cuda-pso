/* 
    OEM-CUDA-PSO - Output Error Method with CUDA and PSO
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CONFIG_H
#define CONFIG_H

typedef struct {
    int group;         // kernel group
    int kernel;        // kernel id
    int dimensions;    // number of dimensions
    int swarms;        // number of swarms
    int particles;     // number of particles
    float c1;          // usually inertia parameter
    float c2;          // usually local minima acceleration
    float c3;          // usually global minima acceleration
    int maxIter;       // maximum number of iterations
    float costLimit;   // exported minima limit value
    int sizeLimit;     // exported minima limit in number por points
    int informantSize; // informantSize    
    bool doExport;     // is used if need to copy the hypercube from device to host
    int threads;       // threads used by reduction processes
    } Config;

/**
 * Print configuration
 */
void printConfig(Config config);

#endif