/* 
    OEM-CUDA - Output Error Method with CUDA
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <math.h>
#include <stdio.h>
#include "../util.h"
#include "../variables.h"
#include "../kernel.h"
#include "../config.h"

__forceinline__ __device__ float windBox(Info info, Variables variables, Position position){    
 
    float windSpeed     = position[0];
    float windDirection = position[1];
    float sensorBias    = position[2];
    float sensorK       = position[3];
    
    float* time               = variable(0, variables);
    float* heading            = variable(1, variables);
    float* dynamicPressure    = variable(2, variables);
    float* staticPressure     = variable(3, variables);
    float* outsideTemperature = variable(4, variables);
    float* realX              = variable(5, variables);
    float* realY              = variable(6, variables);
    
    float x0           = info.values[0];
    float y0           = info.values[1];
    float casIntercept = info.values[2];
    float casK         = info.values[3];
    float l1T          = info.values[4]; // norm 1 of trajectory length
        
    float error = 0.0f;
    float xAnt = x0;
    float yAnt = y0;

    // convert to radians
    const float PI = 3.1415927f;    
    windDirection = windDirection * PI / 180.0;
    float windX = - windSpeed * sinf(windDirection);
    float windY = - windSpeed * cosf(windDirection);

    for(int i=1;i<variables.size;i++){
        // calculate IAS and TAS
        float simDynamicPressure = (dynamicPressure[i-1] - sensorBias) / sensorK;

        if(simDynamicPressure < 0)
            simDynamicPressure = 0;

        float IAS = sqrtf( (2.0f * simDynamicPressure) / 1.225f ); 

        float CAS = IAS * casK + casIntercept; // Calibrated airspeed adjust

        float delta = (288.15f / (outsideTemperature[i-1] + 273.15f)) * (staticPressure[i-1] /101325.0f);
        float TAS = CAS / sqrtf(delta);

        float period = time[i]-time[i-1];

        float angle = heading[i-1];
        float deltaX = (sinf(angle) * TAS + windX) * period;
        float deltaY = (cosf(angle) * TAS + windY) * period;
        float xAct = xAnt + deltaX;
        float yAct = yAnt + deltaY;
        float errorX = xAct - realX[i];
        float errorY = yAct - realY[i];
        error += abs(errorX) + abs(errorY);        
        xAnt = xAct;
        yAnt = yAct;
    }   
    
    return error/l1T;
}
