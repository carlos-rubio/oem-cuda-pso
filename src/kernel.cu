/* 
    OEM-CUDA-PSO - Output Error Method with CUDA and PSO
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "kernel.h"
#include <stdio.h>
#include "macro.h"

/**
 * creates the execution grid
 * 
 *  blocks:  number of independent swarms
 *  threads: number of particles 
 */
Grid createGrid(Config config){    
    dim3 blocks(config.swarms);
    dim3 threads(config.particles);
    Grid grid;
    grid.blocks = blocks;
    grid.threads = threads;
    return grid;
}

/**
 * prints the execution grid
 */
void printGrid(Grid grid){
    dim3 blocks = grid.blocks;
    dim3 threads = grid.threads;
    int totalBlocks = blocks.x * blocks.y * blocks.z;
    int totalThreads = threads.x * threads.y * threads.z;
    printf("\nKernel grid: \n");      
    printf("  Blocks %d, %d, %d, total: %d \n",blocks.x, blocks.y, blocks.z, totalBlocks);      
    printf("  Threads %d, %d, %d, total: %d \n",threads.x, threads.y, threads.z, totalThreads);          
}

/**
 * invokes the kernel given a configuration, parameters, variables, output hypercube and output errors
 * waits for all kernel threads to complete
 * and measure execution time. Return exectution time and kernel error code.
 */
Execution dispatch(Config config, Info info, Variables variables, HypercubeInfo* hypercubes, float* data, float* global){   

    // execution info
    Execution result;

    // Prepare execution grid
    Grid grid = createGrid(config);      
    printGrid(grid);
    
    // time measurement
    cudaEvent_t start, stop;
    HANDLE_CUDA(cudaEventCreate(&start));
    HANDLE_CUDA(cudaEventCreate(&stop));
    HANDLE_CUDA(cudaEventRecord(start, 0)); // start timer 

    // shared memory, [cost, param1, param2, ... paramD] x num particles = 5 floats per particle
    size_t sharedMemorySize = (config.dimensions+1) * sizeof(float) * config.particles;        

    // ---------------------------- Kernel configs --------------------------------
    
    switch(config.group){
        case BASIC_PSO: 
            basicPSO<<<grid.blocks, grid.threads, sharedMemorySize>>>(config, info, variables, hypercubes, data, global);
            break;

        default:
            HANDLE_ERROR(GROUP_ERROR);
    }
    // ---------------------------- Kernel configs --------------------------------

    cudaDeviceSynchronize();    
    
    result.error = cudaGetLastError(); 
    cudaDeviceSynchronize();

    HANDLE_CUDA(cudaEventRecord(stop, 0));
    HANDLE_CUDA(cudaEventSynchronize(stop));
    HANDLE_CUDA(cudaEventElapsedTime(&result.elapsedTime, start, stop));    
    HANDLE_CUDA(cudaEventDestroy(start));
    HANDLE_CUDA(cudaEventDestroy(stop));

    return result;
}

/**
 * gets the group enum value from its name
 */
Group parseGroup(char* name){    
    for(int i=0;i<END_G;i++){
        if(strcmp(name, groupNames[i]) == 0)
            return (Group)i;
    }
    return NONE;
}

/**
 * gets the kernel enum value from its name
 */
Kernel parseKernel(char* name){
    for(int i=0;i<END_K;i++){
        if(strcmp(name, kernelNames[i]) == 0)
            return (Kernel)i;
    }
    return UNKNOWN;
}
