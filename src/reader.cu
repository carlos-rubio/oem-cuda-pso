/* 
    OEM-CUDA-PSO - Output Error Method with CUDA and PSO
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include "reader.h"
#include "memory.h"
#include "macro.h"
#include "kernel.h"
#include "util.h"

static void assureField(char* name, const char* expected);

/**
 * reads and parse input file
 * allocate memory for the input structure and fills it
 */
void readFile(const char* fileName,Input* input){
    FILE *file;
    file = fopen(fileName, "r");
    if(file == NULL){
        printf("Can't open: %s\n", fileName);
        HANDLE_ERROR(IO_ERROR);
    }
    
    char name[250];    
    char key[250];
    
    if(fscanf(file, "%s %s", name, key) != 2) HANDLE_ERROR(FORMAT_ERROR);
    assureField(name, "group:");
    input->group = parseGroup(key);

    if(fscanf(file, "%s %s", name, key) != 2) HANDLE_ERROR(FORMAT_ERROR);
    assureField(name, "kernel:");
    input->kernel = parseKernel(key);

    if(fscanf(file, "%s %i", name, &input->dimensions) != 2) HANDLE_ERROR(FORMAT_ERROR);
    assureField(name, "dimensions:");

    if(fscanf(file, "%s %i", name, &input->swarms) != 2) HANDLE_ERROR(FORMAT_ERROR);
    assureField(name, "swarms:");

    if(fscanf(file, "%s %i", name, &input->particles) != 2) HANDLE_ERROR(FORMAT_ERROR);
    assureField(name, "particles:");

    if(fscanf(file, "%s %f", name, &input->c1) != 2) HANDLE_ERROR(FORMAT_ERROR);
    assureField(name, "c1:");

    if(fscanf(file, "%s %f", name, &input->c2) != 2) HANDLE_ERROR(FORMAT_ERROR);
    assureField(name, "c2:");

    if(fscanf(file, "%s %f", name, &input->c3) != 2) HANDLE_ERROR(FORMAT_ERROR);
    assureField(name, "c3:");

    if(fscanf(file, "%s %i", name, &input->maxIter) != 2) HANDLE_ERROR(FORMAT_ERROR);
    assureField(name, "maxIter:");

    if(fscanf(file, "%s %f", name, &input->costLimit) != 2) HANDLE_ERROR(FORMAT_ERROR);
    assureField(name, "costLimit:");

    if(fscanf(file, "%s %i", name, &input->sizeLimit) != 2) HANDLE_ERROR(FORMAT_ERROR);
    assureField(name, "sizeLimit:");

    if(fscanf(file, "%s %i", name, &input->informantSize) != 2) HANDLE_ERROR(FORMAT_ERROR);
    assureField(name, "informantSize:");

    if(fscanf(file, "%s %i", name, &input->numValues) != 2) HANDLE_ERROR(FORMAT_ERROR);
    assureField(name, "numValues:");

    if(fscanf(file, "%s %i", name, &input->numVariables) != 2) HANDLE_ERROR(FORMAT_ERROR);
    assureField(name, "numVariables:");

    if(fscanf(file, "%s %i", name, &input->sizeVariables) != 2) HANDLE_ERROR(FORMAT_ERROR);
    assureField(name, "sizeVariables:");

    allocMemoryInput(input);

    for(int i=0;i<input->numValues;i++){
        if(fscanf(file, "%s %f", name, &input->parameters[i]) != 2) HANDLE_ERROR(FORMAT_ERROR);
        assureField(name, "value:");
    }

    for(int i=0;i<input->dimensions;i++){
        if(fscanf(file, "%s %f",name, &input->axis[i].min) != 2) HANDLE_ERROR(FORMAT_ERROR);
        assureField(name, "axisMin:");

        if(fscanf(file, "%s %f",name, &input->axis[i].max) != 2) HANDLE_ERROR(FORMAT_ERROR);
        assureField(name, "axisMax:");
    }

    for(int i=0; i < input->sizeVariables ;i++){
        for(int j=0; j<input->numVariables;j++){
            int index = i + j * input->sizeVariables;
            if(fscanf(file, "%f", &input->data[index]) != 1) HANDLE_ERROR(FORMAT_ERROR);
        }        
    } 
    fclose(file);
}

/**
 * checks that the readed value name is the expected one
 */
static void assureField(char* name, const char* expected){
    if(strcmp(trim(name), expected) != 0){
        printf("Error reading input file:\n");
        printf("  found %s\n",name);
        printf("  expected %s\n",expected);
        HANDLE_ERROR(FORMAT_ERROR);
    }
}

/**
 * extracts the configuration part of the Input structure
 */
Config extractConfig(Input input){
    Config config;
    config.group = input.group;
    config.kernel = input.kernel;
    config.swarms = input.swarms;
    config.dimensions = input.dimensions;
    config.particles = input.particles;
    config.c1 = input.c1;
    config.c2 = input.c2;
    config.c3 = input.c3;
    config.maxIter = input.maxIter;    
    config.costLimit = input.costLimit;
    config.sizeLimit = input.sizeLimit;
    config.informantSize = input.informantSize;
    return config;
}

/**
 * extracts the parameters part of the Input structure
 * pre-calculate the slope of each axis line
 * allocate memory for the returned parameters
 */
Info extractParameters(Input input){
    Info parameters;
    parameters.numValues = input.numValues;   
    parameters.numAxis = input.dimensions;
    allocParametersHost(&parameters);

    for(int i=0;i<parameters.numValues;i++){
        parameters.values[i] = input.parameters[i];
    }

    for(int i=0;i<parameters.numAxis;i++){
        parameters.axis[i].min = input.axis[i].min;
        parameters.axis[i].max = input.axis[i].max;    
    }
    return parameters;
}

/**
 * extracts the variables part of the Input structure
 * allocate memory for the returned variables
 */
Variables extractVariables(Input input){
    Variables variables;
    variables.num = input.numVariables;
    variables.size = input.sizeVariables;
    allocVariablesHost(&variables);
    memcpy(variables.data, input.data, variables.num * variables.size * sizeof(float));    
    return variables;
}