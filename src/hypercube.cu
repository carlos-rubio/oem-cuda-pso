/* 
    OEM-CUDA-PSO - Output Error Method with CUDA and PSO
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include "hypercube.h"
#include "variables.h"
#include "macro.h"

/**
 * Exports the hypercubes
 * each point is exported with the format and endianess of the local machine
 * in linux with Intel/AMD64 arch, each point is:
 *   4 bytes, IEE754 simple precision and little-endian
 */

void exportHypercubes(HypercubeInfo* hypercubes, float* data, float* global, const char* fileName, Config config){
    FILE *file;
    file = fopen(fileName, "w");
    if(file == NULL)
        HANDLE_ERROR(IO_ERROR);
        
    if(fwrite(&config.swarms, sizeof(int), 1, file) != 1)
        HANDLE_ERROR(IO_ERROR);

    if(fwrite(global, globalSize(config), 1, file) != 1)
        HANDLE_ERROR(IO_ERROR);

    if(fwrite(hypercubes, hyperInfoSize(config), 1, file) != 1)
        HANDLE_ERROR(IO_ERROR);

    if(fwrite(data, hyperDataSize(config), 1, file) != 1)
        HANDLE_ERROR(IO_ERROR);

    fclose(file);    
}

/**
 * Returns in memory size of the information part of hypercubes object
 */
size_t hyperInfoSize(Config config){
    return config.swarms * sizeof(HypercubeInfo);
}

/**
 * Returns in memory size of the data part of hypercubes object
 */
size_t hyperDataSize(Config config){
    return config.swarms * config.sizeLimit * (config.dimensions + 1) * sizeof(float);
}

/**
 * Returns in memory size to store the minimum position within its cost for each swarm
 */
size_t globalSize(Config config){
    return (config.dimensions + 1) * sizeof(float) * config.swarms;
}

/** 
 * Print a summary of each hypercube
 */
void printSummary(HypercubeInfo* hypercubes, float* data, float* global, Config config){    
    for(int i=0;i<config.swarms;i++){        
        printf("\nSwarm %d:",(i+1));
        printf("\n  Overflow: %d",hypercubes[i].overflow);
        printf("\n  Number of low points: %d",hypercubes[i].used);
        printf("\n  Global minimum:");      
        printf("\n    Cost: %f", global[i*(config.dimensions+1)]);
        for(int j=1;j<=config.dimensions;j++){
            printf("\n    Parameter %d: %f",j,global[j + i*(config.dimensions+1)]);
            }        
    }    
    printf("\n");
}

