/* 
    OEM-CUDA-PSO - Output Error Method with CUDA and PSO
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef KERNEL_H
#define KERNEL_H

#include <stdio.h>
#include "variables.h"
#include "config.h"
#include "hypercube.h"
#include "cost/windBox.h"
#include "cost/windBoxInverse.h"
#include "cost/windBoxBezier.h"

typedef struct {
    dim3 blocks;
    dim3 threads;
} Grid;

typedef struct {
    float elapsedTime;
    cudaError_t error;
} Execution;

// shared memory, [cost, param1, param2, ... paramD] x num particles
extern __shared__ float shared[];

/**
 * Creates the execution grid
 *
 *  blocks:  number of independent swarms
 *  threads: number of particles
 *
 */
Grid createGrid(Config config);

/**
 * prints the execution grid
 */
void printGrid(Grid grid);

/**
 * invokes the kernel given a configuration, parameters, variables, output hypercube
 * waits for all kernel threads to complete
 * and measure execution time. Return exectution time and kernel error code.
 */
Execution dispatch(Config config, Info info, Variables variables, HypercubeInfo* hypercubes, float* data, float* global);

// ---------------------------- Kernel configs --------------------------------

typedef enum Groups { NONE, BASIC_PSO, END_G} Group;
static const char* groupNames[] = {"none", "basic-pso"};

typedef enum Kernels { UNKNOWN, WIND_BOX, WIND_BOX_INVERSE, WIND_BOX_BEZIER, END_K } Kernel;
static const char* kernelNames[] = {"unknown", "windBox", "windBoxInverse","windBoxBezier"};

__global__ void basicPSO(Config config, Info info, Variables variables, HypercubeInfo* hypercubes, float* data, float* global);

// ---------------------------- Kernel configs --------------------------------


// ---------------------------- Device functions --------------------------------

__forceinline__ __device__ float evaluateCost(Config config, Info info, Variables variables, Position position){
    switch(config.kernel){
        case WIND_BOX: 
            return windBox(info, variables, position);
        case WIND_BOX_INVERSE: 
            return windBoxInverse(info, variables, position);
        case WIND_BOX_BEZIER: 
            return windBoxBezier(info, variables, position);
        default:
            return NAN;
    }
}

// ---------------------------- Device functions --------------------------------

/**
 * gets the group enum value from its name
 */
Group parseGroup(char* name);

/**
 * gets the kernel enum value from its name
 */
Kernel parseKernel(char* name);

#endif