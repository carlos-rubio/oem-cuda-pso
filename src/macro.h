/* 
    OEM-CUDA-PSO - Output Error Method with CUDA and PSO
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef MACRO_H
#define MACRO_H

#include <stdio.h>

static const int EXIT_CODE = -1;
enum error {MALLOC_ERROR=1, GROUP_ERROR, KERNEL_ERROR, GPU_ERROR, IO_ERROR, FORMAT_ERROR};

static const char* getErrorString(int err){
    switch(err){
        case MALLOC_ERROR: return "malloc error";
        case GROUP_ERROR: return "group error";
        case KERNEL_ERROR: return "kernel error";
        case GPU_ERROR: return "gpu error";
        case IO_ERROR: return "IO error";
        case FORMAT_ERROR: return "format error";
        default: return "";
    }
}

/**
 * handles APP error, print error name, file and line where occurs
 * and exit
 */
static void HandleError( int err,
                         const char *file,
                         int line ) {
    if (err != 0) {
        printf( "Error: %s in %s at line %d\n", getErrorString(err), file, line );
        exit( EXIT_CODE );
    }
}

#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))

/**
 * handles CUDA error, print error name, file and line where occurs
 * and exit
 */
static void HandleErrorCuda( cudaError_t err,
                         const char *file,
                         int line ) {
    if (err != cudaSuccess) {
        printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
                file, line );
        exit( EXIT_CODE );
    }
}
#define HANDLE_CUDA( err ) (HandleErrorCuda( err, __FILE__, __LINE__ ))

/**
 * handle no GPU detected error
 */
static void HandleNoGpu(){
    printf("Not found any CUDA device or insufficient driver.\n");
    exit( EXIT_CODE );
}

#define HANDLE_NO_GPU() (HandleNoGpu())

#endif