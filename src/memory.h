/* 
    OEM-CUDA-PSO - Output Error Method with CUDA and PSO
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef MEMORY_H
#define MEMORY_H

#include "variables.h"
#include "hypercube.h"

// ------------------ Host ------------------------

void allocMemoryInput(Input* input);

void freeMemoryInput(Input* input);

void allocParametersHost(Info* parameters);

void freeParametersHost(Info* parameters);

void allocHypercubesHost(HypercubeInfo** hypercubesHostPtr, Config config);

void initHypercubesHost(HypercubeInfo* hypercubesHost, Config config);

void freeHypercubesHost(HypercubeInfo* hypercubesHost);

void allocDataHost(float** dataPtr, Config config);

void freeDataHost(float* data);

void allocGlobalHost(float** globalPtr, Config config);

void freeGlobalHost(float* global);

void allocVariablesHost(Variables* variables);

void freeVariablesHost(Variables* variables);


// ----------- Device  --------------------------

void allocHypercubesDevice(HypercubeInfo** hypercubesDevicePtr, Config config);

void freeHypercubesDevice(HypercubeInfo* hypercubesDevice);

void allocDataDevice(float** dataDevicePtr, Config config);

void allocGlobalDevice(float** globalDevicePtr, Config config);

void freeDataDevice(float* dataDevice);

void freeGlobalDevice(float* globalDevice);

void freeVariablesDevice(Variables* variables);

void freeParamsDevice(Info* parametersDevice);

// -------- Copy host to device -------------------

void copyHypercubesToDevice(HypercubeInfo* hypercubesHost, HypercubeInfo* hypercubesDevice, Config config);

void copyParametersToDevice(Info* parameters, Info* parametersDevice);

void copyVariablesToDevice(Variables* variables, Variables* variablesDevice);


// -------- Copy device to host -------------------

void copyHypercubesToHost(HypercubeInfo* hypercubesDevice, HypercubeInfo* hypercubesHost, Config config);

void copyDataToHost(float* dataDevice, float* dataHost, Config config);

void copyGlobalToHost(float* globalDevice, float* globalHost, Config config);

#endif