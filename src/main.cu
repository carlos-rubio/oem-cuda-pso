/* 
    OEM-CUDA-PSO - Output Error Method with CUDA and PSO
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <math.h>
#include <unistd.h>  
#include <string.h>  
#include "kernel.h"
#include "config.h"
#include "macro.h"
#include "reader.h"
#include "memory.h"
#include "variables.h"
#include "device.h"
#include "hypercube.h"
#include "math.h"
#include "util.h"

typedef struct {
    int maxIter;
    float costLimit;
    char* inputFile;
    bool doExport;    
    char* outputFile;    
} ExecConfig;

static void printHelp(void);

static void execute(ExecConfig config);

static void printFormat(void);

static bool handleExecResult(Execution result);

int main(int argc, char **argv) {
    int opt;
    bool valid = false; // valid execution without input file
    
    ExecConfig config;
    config.maxIter = -1;
    config.costLimit = NAN;
    config.doExport = false;       
    config.outputFile = NULL;
   
    while (( opt = getopt(argc, argv, "hfildo:")) != -1){
        switch(opt){
            case 'h': printHelp(); valid = true; break;
            case 'd': printDevices(); valid = true; break;            
            case 'f': printFormat(); valid = true; break;
            case 'i': config.maxIter = atoi(optarg); break;
            case 'l': config.costLimit = atof(optarg); break;
            case 'o': 
                config.doExport = true;
                config.outputFile = strdup(optarg);
                break;            
            default:                 
                printHelp(); return -1; // incorrect option
        }
    }

    if (optind >= argc && !valid) {
        printHelp(); return -1; // don't exists input file name
    }

    if(optind < argc) {
        config.inputFile = argv[optind];
    
        execute(config); 

        if(config.outputFile != NULL)
            free(config.outputFile);
    }
    return 0;
}

static void printHelp(void){
    printf("\nUsage: oem-pso [OPTION]... INFILE\n");
    printf("\nOptions:\n");
    printf("\n  -h        show help\n");
    printf("\n  -f        show INFILE required format\n");    
    printf("\n  -d        show GPU devices\n");
    printf("\n  -i iters  override max iterations\n");
    printf("\n  -l limit  override cost limit to export\n");
    printf("\n  -o file   export minimum locations to raw file\n");
    printf("\n");
}

static void printFormat(void){
    printf("\nFormat of input file:\n\n");
    printf("  group: groupName\n");
    printf("  kernel: kernelName\n");
    printf("  dimensions: d\n");
    printf("  particles: p\n");
    printf("  c1: c1\n");
    printf("  c2: c2\n");
    printf("  maxIter: mi\n");
    printf("  costLimit: cl\n");
    printf("  sizeLimit: sl (bytes)\n");
    printf("  informantSize: is\n");
    printf("  numValues: n\n");
    printf("  numVariables: m\n");    
    printf("  sizeVariables: l\n\n");
    printf("  value: value-1\n");
    printf("  value: value-2\n");
    printf("  ...\n");
    printf("  value: value-n\n\n");
    printf("  axisMin: axisMin-1\n");
    printf("  axisMax: axisMax-1\n");
    printf("  ...\n");
    printf("  axisMin: axisMin-s\n");
    printf("  axisMax: axisMax-s\n\n");
    printf("  1.0 1.0 1.0 ... 1.0 (data of m variables, index 1)\n");
    printf("  ...\n");
    printf("  1.0 1.0 1.0 ... 1.0 (data of m variables, index l)\n");
    printf("\n");
}

static void execute(ExecConfig execConfig){
    // select and print device info
    int device = selectBestDevice();
    printDevice(device);
    HANDLE_CUDA(cudaSetDevice(device));    
    int maxThreadsPerBlock = getMaxThreadsPerBlock(device);

    // read input (also allocate input memory in host)
    Input input;
    readFile(execConfig.inputFile, &input);

    // read configuration
    Config config = extractConfig(input);    
    config.doExport = execConfig.doExport;
    config.threads = maxThreadsPerBlock;
    // override maxIter from parameter -i
    if(execConfig.maxIter != -1) config.maxIter = execConfig.maxIter;
    // override costLimit from parameter -l
    if(!isnan(execConfig.costLimit)) config.costLimit = execConfig.costLimit;    
    // show configuration
    printConfig(config);

    // read parameters, axis limits and variables
    Info parameters = extractParameters(input);
    Variables variables = extractVariables(input);
   
    // copy variables to device memory    
    Variables variablesDevice = variables;
    copyVariablesToDevice(&variables, &variablesDevice);

    // copy parameters to device memory
    Info parametersDevice = parameters;
    copyParametersToDevice(&parameters, &parametersDevice);

    // create hypercubes in host memory
    HypercubeInfo* hypercubes; // host hypercubes
    allocHypercubesHost(&hypercubes, config);
    initHypercubesHost(hypercubes, config);

    // create hypercubes data in host memory
    float* data;
    allocDataHost(&data, config);

    // create struct to store global minimum in host memory
    float* global;
    allocGlobalHost(&global, config);
    
    // create hypercube in device
    HypercubeInfo* hypercubesDevice;
    allocHypercubesDevice(&hypercubesDevice, config); 

    // create data in device
    float* dataDevice;
    allocDataDevice(&dataDevice, config);

    // create struct to store global minimum in device
    float* globalDevice;
    allocGlobalDevice(&globalDevice, config);
    
    // Copy information part from host to device (initialization)
    copyHypercubesToDevice(hypercubes, hypercubesDevice, config);

    bool ok = true;
    Execution exec;

    // invoke the kernel
    exec = dispatch(config, parametersDevice, variablesDevice, hypercubesDevice, dataDevice, globalDevice);   
    ok = handleExecResult(exec);             

    if(ok) {        
        // copy device hypercubes to host
        copyHypercubesToHost(hypercubesDevice, hypercubes, config);
        // copy hypercube data to host
        copyDataToHost(dataDevice, data, config);
        // copy global minimum to host
        copyGlobalToHost(globalDevice, global, config);
        // free hypercubes device memory
        freeHypercubesDevice(hypercubesDevice);
        // free data device memory
        freeDataDevice(dataDevice);    
        // free global minimum device memory
        freeDataDevice(globalDevice);    
    } 

    // free device memory
    
    freeVariablesDevice(&variablesDevice);
    freeParamsDevice(&parametersDevice);
  
    if(ok){
        // print time
        char buffer[50];
        formatTime(buffer, exec.elapsedTime);
        printf("\nElapsed time: %s\n", buffer);
        
        // print summary of each hypercube
        printSummary(hypercubes, data, global, config);
        
        // export hypercube as raw data
        if(execConfig.doExport){                    
            exportHypercubes(hypercubes, data, global, execConfig.outputFile, config);    
            printf("\nExported hypercubes to: %s\n", execConfig.outputFile);
        }
    }

    // Free host memory
    freeMemoryInput(&input);     
    freeParametersHost(&parameters);      

    if(ok) {  
        freeHypercubesHost(hypercubes);
        freeDataHost(data);
        freeGlobalHost(global);
    }
}

/**
 * checks execution result error
 */
static bool handleExecResult(Execution result){
    if(result.error == cudaSuccess) return true;

    printf("\nError invoking kernel: %s\n", cudaGetErrorString (result.error) );

    if(result.error == cudaErrorInvalidValue){
        printf("   Check CUDAFLAGS parameter in makefile.\n");
        printf("   NVCC uses -arch=sm_20 by default.\n");        
        printf("   Update to match your device and compile again.\n");        
    }
    return false;
}

