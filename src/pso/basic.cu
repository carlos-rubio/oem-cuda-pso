/* 
    OEM-CUDA - Output Error Method with CUDA
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <math.h>
#include <stdio.h>
#include <curand_kernel.h>
#include "../variables.h"
#include "../hypercube.h"
#include "../kernel.h"
#include "../config.h"
#include "../util.h"

__global__ void basicPSO(Config config, Info info, Variables variables, HypercubeInfo* hypercubes, float* data, float* global){    

    int iterations = 0;

    int swarm = blockIdx.x;
    int particle = threadIdx.x;
    int blockSize = config.dimensions + 1; // size needed to store the cost and the position of a point
    int hypercubeSize = config.sizeLimit * blockSize; // number of floats in one hypercube 

    // Initialize CUDA random
    curandState randomState;    
    int id = threadIdx.x + blockIdx.x * blockDim.x;
    curand_init (clock64(), id, 0, &randomState);

    // sync all threads
    __syncthreads();

    // Initialize particle
    Position position  = new float[config.dimensions];    
    Velocity velocity  = new float[config.dimensions];
    Position bestLocal = new float[config.dimensions];
    float bestLocalCost = NAN;

    for(int d=0;d<config.dimensions;d++){
        float min = info.axis[d].min;
        float max = info.axis[d].max;
        position[d] = randomUniform(&randomState, min, max);
        velocity[d] = randomUniform(&randomState, (min-max)/2.0f, (max-min)/2.0f);
    }

    while(iterations < config.maxIter){
        // Evaluate cost
        float cost = evaluateCost(config, info, variables, position);

        // Update local minimum
        if(iterations == 0 || cost < bestLocalCost){
            bestLocalCost = cost;
            copyPosition(position, bestLocal, config.dimensions);
        }

        // Store the low points in hypercube
        // base is the position in data array where the hypercube of the swarm starts
        int base = hypercubeSize*swarm;  
        if(cost < config.costLimit){
            // get and lock free position
            int free = atomicAdd(&hypercubes[swarm].used,1);
            if(free < config.sizeLimit){
                int freeIndex = base + free * blockSize;
                // store cost
                data[freeIndex] = cost;
                // store parameters
                copyPosition(position, &data[freeIndex+1], config.dimensions);
            } else  {
                hypercubes[swarm].overflow = true;
            }
        }            

        // Update known global minimum of next n particles
        for(int i=1;i<=config.informantSize;i++){
            int informedParticle = (particle+i)%config.particles;                        
            int index = informedParticle * (config.dimensions + 1);            
            if(cost < shared[index] || (iterations == 0 && i==1)){
                shared[index] = cost;
                copyPosition(position, &shared[index+1], config.dimensions);
            }            
            // sync all threads
            __syncthreads();
        }  

        // Index in shared memory to get the particle known best global position
        int sharedIndex = particle * blockSize;
        // Move to next position
        for(int d=0;d<config.dimensions;d++){            
            velocity[d] = config.c1 * velocity[d] // Inertia term
                        + randomUniform(&randomState, 0, config.c2) * (bestLocal[d] - position[d]) // local acceleration term
                        + randomUniform(&randomState, 0, config.c3) * (shared[sharedIndex+1+d] - position[d]); // global acceleration term
            position[d] = position[d] + velocity[d];
        }

        // Interval confinement
        for(int d=0;d<config.dimensions;d++){
            float min = info.axis[d].min;
            float max = info.axis[d].max;
            if(position[d] < min){ velocity[d] = 0.0f; position[d] = min; }
            if(position[d] > max){ velocity[d] = 0.0f; position[d] = max; }
        }

        // increment iteration number
        ++iterations;

        // sync all threads
        __syncthreads();
    }

    // reduction process to obtain the global minimum fro shared memory
    int pending = config.particles;
    while(pending > 1){
        pending = (pending + 1) / 2;
        if(particle < pending){
            int indexFirst = particle * blockSize;
            int indexSecond = (particle + pending) * blockSize;

            // consolidate error for threadIndex and threadIndex + pending
            if( particle < config.particles && shared[indexSecond] < shared[indexFirst]){
                shared[indexFirst] = shared[indexSecond]; // copy cost
                copyPosition(&shared[indexSecond+1], &shared[indexFirst+1], config.dimensions);                
            }
        }
        __syncthreads();
    }

    if(particle == 0){ // store global minimum
        for(int i=0;i<=config.dimensions;i++){ // store cost and parameters
            global[i + blockSize*swarm ] = shared[i];
        }        
    }

    // free local allocated memory
    delete position;
    delete velocity;
    delete bestLocal;
}
