/* 
    OEM-CUDA-PSO - Output Error Method with CUDA and PSO
    Copyright (C) 2019 Carlos Rubio <carlos@xnor.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef VARIABLES_H
#define VARIABLES_H

#include "config.h"

typedef float* Variable;

typedef float* Position;

typedef float* Velocity;

// parameter axis
typedef struct {
    float min; // axis minimum (one end of the axis)
    float max; // axis maximum (the other end)    
} Axis;

typedef struct {
    int group;         // kernel group
    int kernel;        // kernel id
    int dimensions;    // number of dimensions
    int swarms;        // number of swarms
    int particles;     // number of particles
    float c1;          // inertia parameter
    float c2;          // local acceleration parameter
    float c3;          // global acceleration parameter
    int maxIter;       // maximum number of iterations
    float costLimit;   // exported minima limit value
    int sizeLimit;     // exported minima limit in number por points
    int informantSize; // informantSize
    int numValues;     // num values in the input fie
    int numVariables;  // num of variables in the input file
    int sizeVariables; // num of values in each variable
    float* parameters; // parameters (values)
    Axis* axis;   // axis data (min and max values of each axis)
    float* data;       // variables data
    } Input; // store the readed information from the input file

typedef struct {
    int num; // num of variables
    int size; // num of values in each variable
    float* data; // variables data
 } Variables; // variables as they are sent to the kernel

typedef struct {
    int numValues; // num of values
    float* values; // parameters (values) data
    int numAxis;   // num of axis
    Axis* axis;    // axis data
} Info; // parameters and axis as they are sent to the kernel

#endif