# OEM-CUDA-PSO

OEM-CUDA-PSO is a parallel particle swarm optimization solver for system identification problems that runs on a NVIDIA GPU.

## Running

Your kernel need to be compiled with the rest of the framework:

[Download and compile](https://bitbucket.org/carlos-rubio/oem-cuda-pso/wiki/Download%20and%20compile)

